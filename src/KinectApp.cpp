/*
* 
* Copyright (c) 2013, Ban the Rewind
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or 
* without modification, are permitted provided that the following 
* conditions are met:
* 
* Redistributions of source code must retain the above copyright 
* notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright 
* notice, this list of conditions and the following disclaimer in 
* the documentation and/or other materials provided with the 
* distribution.
* 
* Neither the name of the Ban the Rewind nor the names of its 
* contributors may be used to endorse or promote products 
* derived from this software without specific prior written 
* permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* 
*/

// Includes
#include <algorithm>
#include <list>
#include "boost/algorithm/string.hpp"
#include "cinder/app/AppBasic.h"
#include "cinder/Camera.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "cinder/TriMesh.h"
#include "cinder/ImageIo.h"
#include "cinder/params/Params.h"
#include "cinder/Vector.h"
#include "cinder/Utilities.h"
#include "cinder/Perlin.h"
#include "Kinect.h"
#include "Resources.h"

/*
* This application explores the features of the Kinect SDK wrapper. It 
* demonstrates how to start a device, query for devices, adjust tilt, 
* and read and represent color, depth, and skeleton data.  
* It's also useful as a device test and control panel.
*/
class KinectApp : public ci::app::AppBasic 
{
public:
	void								draw();
	void								prepareSettings( ci::app::AppBasic::Settings *settings );
	void								setup();
	void								shutdown();
	void								update();
	void								setVertices();
private:
	// Capturing flags
	bool								mCapture;
	bool								mCapturePrev;
	bool								mBinaryMode;
	bool								mBinaryModePrev;
	bool								mEnabledColor;
	bool								mEnabledColorPrev;
	bool								mEnabledDepth;
	bool								mEnabledDepthPrev;
	bool								mEnabledNearMode;
	bool								mEnabledNearModePrev;
	bool								mEnabledSeatedMode;
	bool								mEnabledSeatedModePrev;
	bool								mEnabledSkeletons;
	bool								mEnabledSkeletonsPrev;
	bool								mEnabledStats;
	bool								mFlipped;
	bool								mFlippedPrev;
	bool								mInverted;
	bool								mInvertedPrev;

	// Kinect
	ci::Surface8u						mColorSurface;
	ci::Surface16u						mDepthSurface;
	int32_t								mDeviceCount;
	KinectSdk::DeviceOptions			mDeviceOptions;
	KinectSdk::KinectRef				mKinect;
	std::vector<KinectSdk::Skeleton>	mSkeletons;
	int32_t								mTilt;
	int32_t								mTiltPrev;
	int32_t								mUserCount;
	void								startKinect();

	// Kinect callbacks
	int32_t								mCallbackDepthId;
	int32_t								mCallbackSkeletonId;
	int32_t								mCallbackColorId;
	void								onColorData( ci::Surface8u surface, const KinectSdk::DeviceOptions& deviceOptions );
	void								onDepthData( ci::Surface16u surface, const KinectSdk::DeviceOptions& deviceOptions );
	void								onSkeletonData( std::vector<KinectSdk::Skeleton> skeletons, const KinectSdk::DeviceOptions& deviceOptions );

	// Camera
	ci::CameraPersp						mCamera;

	// Params
	float								mFrameRateApp;
	float								mFrameRateColor;
	float								mFrameRateDepth;
	float								mFrameRateSkeletons;
	bool								mFullScreen;
	ci::params::InterfaceGlRef			mParams; 
	bool								mRemoveBackground;
	bool								mRemoveBackgroundPrev;
	void								resetStats();

	// Save screen shot
	void								screenShot();

	// JN
	cinder::gl::Texture					mSparkTexture;
	cinder::gl::Texture					mLogoTexture;
	std::vector<ci::Color>				mBrandColors;
	cinder::Vec3i						mJointsForTriangles[18];
	std::map<int, cinder::Vec3f>		mVertexJoints;
	cinder::TriMesh						mTriangles;
	ci::Surface8u						mGreenSurface;
};

// Imports
using namespace ci;
using namespace ci::app;
using namespace KinectSdk;
using namespace std;

// Render
void KinectApp::draw()
{
	// Clear window
	gl::setViewport( getWindowBounds() );
	
	// clear to white for multiplied background
	gl::clear( Colorf::gray( 1.0f ) );
	
	// We're capturing
	if ( mKinect->isCapturing() ) {

		// Set up camera for 3D
		gl::setMatrices( mCamera );
		
		// Switch to 2D
		gl::pushMatrices();
		gl::setMatricesWindow( getWindowSize(), true );
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_SRC_COLOR);
		// Multiply blend mode
		//gl::enable(GL_BLEND);
		//glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);

		// Draw depth and color textures
		//gl::color( Colorf::white() );
		/*if ( mDepthSurface ) {
			Area srcArea( 0, 0, mDepthSurface.getWidth(), mDepthSurface.getHeight() );
			Rectf destRect( 265.0f, 15.0f, 505.0f, 195.0f );
			gl::draw( gl::Texture( mDepthSurface ), srcArea, destRect );
		}*/
		if ( mColorSurface ) {
			Area srcArea( 0, 0, mColorSurface.getWidth(), mColorSurface.getHeight() );
			Channel mGreySurface(mColorSurface);
			/*
			// adjsut contrast (basic)
			Channel::Iter chIter = mGreySurface.getIter();
			int threshold = 127;
			while(chIter.line()){
				while(chIter.pixel()){
					if(chIter.v() * 1.45 > 255){
						chIter.v() = 255;
					} else {
						chIter.v() *= 1.45;
					}
				}
			}*/
			
			// set up the green overlay
			/*mGreenSurface = Surface8u( mColorSurface.getWidth(), mColorSurface.getHeight(), false, SurfaceChannelOrder::RGB );
			Surface::Iter iter = mGreenSurface.getIter();
			while( iter.line() ) {
				while( iter.pixel() ) {
					iter.r() = 51;
					iter.g() = 189;
					iter.b() = 73;
				}
			}*/

			//Area grnArea(0, 0, mGreenSurface.getWidth(), mGreenSurface.getHeight() );
			Area logoArea(0, 0, 1920, 963);
			//Rectf grnDestRect( 0.0f, 0.0f, getWindowSize().x, getWindowSize().x*0.75);
			Rectf destRect( 0.0f, 0.0f, getWindowSize().x, getWindowSize().x*0.75 );
			gl::draw( gl::Texture( mGreySurface ), srcArea, destRect );
			//gl::draw( gl::Texture( mGreenSurface ), grnArea, grnDestRect );

			// mask video feed
			gl::popMatrices();
			gl::pushMatrices();
			gl::scale( Vec3f( 3.0, 3.0, 3.0) );

			mTriangles.clear();
			/*console() << "vertex 1: " << mJointsForTriangles[0].x << endl;
			console() << "coords: " << mVertexJoints[mJointsForTriangles[0].x] << endl;
			console() << "vertex 2: " << mJointsForTriangles[0].y << endl;
			console() << "coords: " << mVertexJoints[mJointsForTriangles[0].y] << endl;
			console() << "vertex 3: " << mJointsForTriangles[0].z << endl;
			console() << "coords: " << mVertexJoints[mJointsForTriangles[0].z] << endl;
			*/

			for(int j=0; j < 10;j++){
				cinder::ColorA c = ColorA(mBrandColors[j%6].r/255,mBrandColors[j%6].g/255,mBrandColors[j%6].b/255,0.1f);
					
				Vec3f v = mVertexJoints[mJointsForTriangles[j].x];
				mTriangles.appendVertex(Vec3f(v.x,v.y,v.z));
				mTriangles.appendColorRgba(c);

				v = mVertexJoints[mJointsForTriangles[j].y];
				mTriangles.appendVertex(Vec3f(v.x,v.y,v.z));
				mTriangles.appendColorRgba(c);

				v = mVertexJoints[mJointsForTriangles[j].z];
				mTriangles.appendVertex(Vec3f(v.x,v.y,v.z));
				mTriangles.appendColorRgba(c);

				mTriangles.appendTriangle(j*3,(j*3)+1,(j*3)+2);
				//mTriangles.appendColorRgb(Color(51,179,83));
			}
				
			gl::draw(mTriangles);
			
			// Switch to 2D
			gl::pushMatrices();
			gl::setMatricesWindow( getWindowSize(), true );

			// draw nurun logo
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			Rectf logoRect ( 0.0f, 0.0f, getWindowSize().x, getWindowSize().x/2 );
			gl::draw( mLogoTexture, logoRect );
		}
		//gl::disable(GL_BLEND);
		
		
		// Normal blend mode
		//glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		// Move skeletons down below the rest of the interface
		gl::popMatrices();
		gl::pushMatrices();
		gl::scale( Vec3f( 3.0, 3.0, 3.0) );//Vec2f::one() * 2.0f );
		/*gl::translate( 0.0f, -0.62f, 0.0f );*/


		// Iterate through skeletons
		uint32_t i = 0;
		for ( vector<Skeleton>::const_iterator skeletonIt = mSkeletons.cbegin(); skeletonIt != mSkeletons.cend(); ++skeletonIt, i++ ) {

			// Set color
			//gl::color( mKinect->getUserColor( i ) );
			
			// Draw bones and joints
			for ( Skeleton::const_iterator boneIt = skeletonIt->cbegin(); boneIt != skeletonIt->cend(); ++boneIt ) {
					
				// Get positions of each joint in this bone to draw it
				const Bone& bone	= boneIt->second;
				double boneZ = bone.getPosition().z;
				boneZ *= -1;
				Vec3f position = Vec3f( bone.getPosition().x, bone.getPosition().y, boneZ);
				position *= 4.5f;
				Vec3f destinationUnflipped	= skeletonIt->at( bone.getStartJoint() ).getPosition();
				boneZ = destinationUnflipped.z *= -1;
				Vec3d destination	= Vec3f( destinationUnflipped.x, destinationUnflipped.y, boneZ);
				destination *= 4.5f;
				int jointIndex		= bone.getEndJoint();

				// if we're at the head
				if(jointIndex == 3){
					//gl::color(Color(255, 255, 255));
					mSparkTexture.enableAndBind();
					Vec3f sparkPos = Vec3f( position.x + 0.3f, position.y + 0.0f, position.z);
					gl::drawBillboard( sparkPos, Vec2f(1.2f,1.2f), 0.0f, Vec3f(1,0,0), Vec3f(0,1,0));
					mSparkTexture.disable();
					mSparkTexture.unbind();
					// console() << "z: " << position.z << endl;
					//gl::color( mKinect->getUserColor( i ) );
				}

				// Draw joint
				//gl::drawSphere( position, 0.025f, 16 );

				// Draw bone
				//gl::drawLine( position, destination );
					
				// Create map of joint positions
				//mVertexJoints[jointIndex] = destination;
				//console() << "joint " << jointIndex << ", position " << destination << ", filed as: " << mVertexJoints[jointIndex] << endl;
			}

			//if(mUserCount > 0){
				// Create vertex list for triangles
				//mTriangles.clear();
				/*console() << "vertex 1: " << mJointsForTriangles[0].x << endl;
				console() << "coords: " << mVertexJoints[mJointsForTriangles[0].x] << endl;
				console() << "vertex 2: " << mJointsForTriangles[0].y << endl;
				console() << "coords: " << mVertexJoints[mJointsForTriangles[0].y] << endl;
				console() << "vertex 3: " << mJointsForTriangles[0].z << endl;
				console() << "coords: " << mVertexJoints[mJointsForTriangles[0].z] << endl;
				*/
				/*
				for(int j=0; j < 6;j++){
					cinder::ColorA c = ColorA(mBrandColors[j%6].r/255,mBrandColors[j%6].g/255,mBrandColors[j%6].b/255,0.5f);
					
					Vec3f v = mVertexJoints[mJointsForTriangles[j].x];
					mTriangles.appendVertex(Vec3f(v.x,v.y,v.z));
					mTriangles.appendColorRgba(c);

					v = mVertexJoints[mJointsForTriangles[j].y];
					mTriangles.appendVertex(Vec3f(v.x,v.y,v.z));
					mTriangles.appendColorRgba(c);

					v = mVertexJoints[mJointsForTriangles[j].z];
					mTriangles.appendVertex(Vec3f(v.x,v.y,v.z));
					mTriangles.appendColorRgba(c);

					mTriangles.appendTriangle(j*3,(j*3)+1,(j*3)+2);
					//mTriangles.appendColorRgb(Color(51,179,83));
				}
				
				gl::draw(mTriangles);*/
			/*} else {
				mTriangles.clear();
			}*/

		}

		// Switch to 2D
		gl::popMatrices();
		gl::setMatricesWindow( getWindowSize(), true );
		
	}
	// Draw the interface
	mParams->draw();
}

// Receives color data
void KinectApp::onColorData( Surface8u surface, const DeviceOptions& deviceOptions )
{
	mColorSurface = surface;
}

// Receives depth data
void KinectApp::onDepthData( Surface16u surface, const DeviceOptions& deviceOptions )
{
	mDepthSurface = surface;
}

// Receives skeleton data
void KinectApp::onSkeletonData( vector<Skeleton> skeletons, const DeviceOptions& deviceOptions )
{
	mSkeletons = skeletons;
}

// Prepare window
void KinectApp::prepareSettings( Settings *settings )
{
	settings->setWindowSize( 1005, 570 );
	settings->setFrameRate( 60.0f );
}

// Reset statistics
void KinectApp::resetStats()
{
	mFrameRateDepth		= 0.0f;
	mFrameRateSkeletons	= 0.0f;
	mFrameRateColor		= 0.0f;
	mUserCount			= 0;
}

// Take screen shot
void KinectApp::screenShot()
{
	writeImage( getAppPath() / fs::path( "frame" + toString( getElapsedFrames() ) + ".png" ), copyWindowSurface() );
}

// Set up
void KinectApp::setup()
{
	// Set up OpenGL
	glLineWidth( 2.0f );
	gl::color( ColorAf::white() );

	// Set up camera
	mCamera.lookAt( Vec3f( 0.0f, 0.0f, 4.0f ), Vec3f::zero() );
	mCamera.setPerspective( 43.0f, getWindowAspectRatio(), 1.0f, 3000.0f );

	// Initialize parameters
	mBinaryMode				= false;
	mBinaryModePrev			= mBinaryMode;
	mCapture				= true;
	mCapturePrev			= mCapture;
	mDeviceCount			= 0;
	mEnabledColor			= true;
	mEnabledColorPrev		= mEnabledColor;
	mEnabledDepth			= true;
	mEnabledDepthPrev		= mEnabledDepth;
	mEnabledNearMode		= false;
	mEnabledNearModePrev	= mEnabledNearMode;
	mEnabledSeatedMode		= false;
	mEnabledSeatedModePrev	= mEnabledSeatedMode;
	mEnabledSkeletons		= true;
	mEnabledSkeletonsPrev	= mEnabledSkeletons;
	mEnabledStats			= true;
	mFlipped				= false;
	mFlippedPrev			= mFlipped;
	mFrameRateApp			= 0.0f;
	mFrameRateDepth			= 0.0f;
	mFrameRateSkeletons		= 0.0f;
	mFrameRateColor			= 0.0f;
	mFullScreen				= isFullScreen();
	mInverted				= false;
	mInvertedPrev			= mInverted;
	mRemoveBackground		= false;
	mRemoveBackgroundPrev	= mRemoveBackground;
	mUserCount				= 0;

	// Start image capture
	mKinect = Kinect::create();
	startKinect();

	// Add callbacks
	mCallbackDepthId	= mKinect->addDepthCallback( &KinectApp::onDepthData, this );
	mCallbackSkeletonId	= mKinect->addSkeletonTrackingCallback( &KinectApp::onSkeletonData, this );
	mCallbackColorId	= mKinect->addColorCallback( &KinectApp::onColorData, this );

	// Setup the parameters
	mParams = params::InterfaceGl::create( getWindow(), "Parameters", toPixels( Vec2i( 245, 500 ) ) ); 
	mParams->addText( "DEVICE" );
	mParams->addParam( "Device count",			&mDeviceCount,							"", true				);
	mParams->addParam( "Device angle",			&mTilt,									"min=-" + 
		toString( Kinect::MAXIMUM_TILT_ANGLE ) + " max=" + toString( Kinect::MAXIMUM_TILT_ANGLE ) + " step=1"	);
	mParams->addSeparator();
	mParams->addText( "STATISTICS");
	mParams->addParam( "Collect statistics",		&mEnabledStats,							"key=t"					);
	mParams->addParam( "App frame rate",			&mFrameRateApp,							"", true				);
	mParams->addParam( "Color frame rate",		&mFrameRateColor,						"", true				);
	mParams->addParam( "Depth frame rate",		&mFrameRateDepth,						"", true				);
	mParams->addParam( "Skeleton frame rate",	&mFrameRateSkeletons,					"", true				);
	mParams->addParam( "User count",				&mUserCount,							"", true				);
	mParams->addSeparator();
	mParams->addText( "CAPTURE" );
	mParams->addParam( "Capture",				&mCapture,								"key=c" 				);
	mParams->addParam( "Depth",					&mEnabledDepth,							"key=d" 				);
	mParams->addParam( "Skeletons",				&mEnabledSkeletons,						"key=k" 				);
	mParams->addParam( "Color",					&mEnabledColor,							"key=v" 				);
	mParams->addSeparator();
	mParams->addText( "INPUT");
	mParams->addParam( "Remove background",		&mRemoveBackground,						"key=b" 				);
	mParams->addParam( "Binary depth mode",		&mBinaryMode,							"key=w" 				);
	mParams->addParam( "Invert binary image",	&mInverted,								"key=i" 				);
	mParams->addParam( "Flip input",				&mFlipped,								"key=m" 				);
	mParams->addParam( "Near mode",				&mEnabledNearMode,						"key=n" 				);
	mParams->addParam( "Seated mode",			&mEnabledSeatedMode,					"key=e" 				);
	mParams->addSeparator();
	mParams->addText( "APPLICATION" );
	mParams->addParam( "Full screen",			&mFullScreen,							"key=f"					);
	mParams->addButton( "Screen shot",			bind( &KinectApp::screenShot, this ),	"key=s"					);
	mParams->addButton( "Quit",					bind( &KinectApp::quit, this ),			"key=q"				);
	mParams->addSeparator();
	mParams->addText( "JN" );
	mParams->addButton( "Reset Triangles",		bind( &KinectApp::setVertices, this),	"key=t"					);

	// load the textures
	//  (note: enable mip-mapping by default)
	gl::Texture::Format format1, format2;
	format1.enableMipmapping(true);			
	ImageSourceRef img1 = loadImage( loadResource( RES_SPARK, "IMAGE" ) );
	if(img1) mSparkTexture = gl::Texture( img1, format1 );
	ImageSourceRef img2 = loadImage( loadResource( RES_LOGO, "IMAGE" ) );
	if(img2) mLogoTexture = gl::Texture( img2, format2 );

	// Set the color palette
	mBrandColors.push_back(Color(51,179,83));
	mBrandColors.push_back(Color(0,0,0));
	mBrandColors.push_back(Color(51,179,83));
	mBrandColors.push_back(Color(51,189,184));
	mBrandColors.push_back(Color(0,102,51));
	mBrandColors.push_back(Color(153,204,51));
	
	srand (time(NULL));

	// Set the joints to use for random triangles
	for(int i=0; i < 18; i++){
		mJointsForTriangles[i] = Vec3i(rand()%(18+1),rand()%(18+1),rand()%(18+1));
	}

	// Set the vertices for random triangles
	mVertexJoints = std::map<int, cinder::Vec3f>();
	
	setVertices();

	//console() << getWindowWidth() << ", " << getWindowHeight() << endl;
}


// Quit
void KinectApp::shutdown()
{
	// Stop input
	mKinect->removeCallback( mCallbackDepthId );
	mKinect->removeCallback( mCallbackSkeletonId );
	mKinect->removeCallback( mCallbackColorId );
	mKinect->stop();
	mSkeletons.clear();
}

// Start Kinect input
void KinectApp::startKinect()
{
	// Update device count
	mDeviceCount = Kinect::getDeviceCount();
	
	// Configure device
	mDeviceOptions.enableDepth( mEnabledDepth );
	mDeviceOptions.enableNearMode( mEnabledNearMode );
	mDeviceOptions.enableSkeletonTracking( mEnabledSkeletons, mEnabledSeatedMode );
	mDeviceOptions.enableColor( mEnabledColor );
	mKinect->enableBinaryMode( mBinaryMode );
	mKinect->removeBackground( mRemoveBackground );
	mKinect->setFlipped( mFlipped );

	// Stop, if capturing
	if ( mKinect->isCapturing() ) {
		mKinect->stop();
	}

	// Start Kinect
	mKinect->start( mDeviceOptions );
	
	// Trace out the unique device ID
	console() << "Device ID: " << mKinect->getDeviceOptions().getDeviceId() << endl;

	// Get device angle angle
	mTilt = mKinect->getTilt();
	mTiltPrev = mTilt;

	// Clear stats
	resetStats();
}

// Runs update logic
void KinectApp::update()
{
	// Update frame rate
	mFrameRateApp = getAverageFps();

	// Toggle fullscreen
	if ( mFullScreen != isFullScreen() ) {
		setFullScreen( mFullScreen );
	}

	// Toggle background remove
	if ( mRemoveBackground != mRemoveBackgroundPrev ) {
		mKinect->removeBackground( mRemoveBackground );
		mRemoveBackgroundPrev = mRemoveBackground;
	}

	// Toggle mirror image
	if ( mFlipped != mFlippedPrev ) {
		mKinect->setFlipped( mFlipped );
		mFlippedPrev = mFlipped;
	}

	// Toggle capture
	if ( mCapture != mCapturePrev ) {
		mCapturePrev = mCapture;
		if ( mCapture ) {
			startKinect();
		} else {
			mKinect->stop();
		}
	}

	// Toggle input tracking types (requires device restart)
	if ( mEnabledColor		!= mEnabledColorPrev		|| 
		mEnabledDepth		!= mEnabledDepthPrev		|| 
		mEnabledNearMode	!= mEnabledNearModePrev		|| 
		mEnabledSeatedMode	!= mEnabledSeatedModePrev	|| 
		mEnabledSkeletons	!= mEnabledSkeletonsPrev	) {
		startKinect();
		mEnabledColorPrev		= mEnabledColor;
		mEnabledDepthPrev		= mEnabledDepth;
		mEnabledNearModePrev	= mEnabledNearMode;
		mEnabledSeatedModePrev	= mEnabledSeatedMode;
		mEnabledSkeletonsPrev	= mEnabledSkeletons;
	}

	// Toggle binary mode
	if ( mBinaryMode	!= mBinaryModePrev	|| 
		mInverted		!= mInvertedPrev ) {
		mKinect->enableBinaryMode( mBinaryMode, mInverted );
		mBinaryModePrev	= mBinaryMode;
		mInvertedPrev	= mInverted;
	}

	// Check if device is capturing
	if ( mKinect->isCapturing() ) {

		// Update device
		mKinect->update();

		// Adjust Kinect camera angle, as needed
		if ( mTilt != mTiltPrev ) {
			mKinect->setTilt( mTilt );
			mTiltPrev = mTilt;
		}

		// Statistics enabled (turn off to improve performance)
		if ( mEnabledStats ) {

			// Update user count
			mUserCount			= mKinect->getUserCount();
		
			// Update frame rates
			mFrameRateColor		= mKinect->getColorFrameRate();
			mFrameRateDepth		= mKinect->getDepthFrameRate();
			mFrameRateSkeletons	= mKinect->getSkeletonFrameRate();
			
		} else {

			// Clear stats
			resetStats();

		}

	} else {

		// If Kinect initialization failed, try again every 90 frames
		if ( getElapsedFrames() % 90 == 0 ) {
			mKinect->start();
		}

	}
}

void KinectApp::setVertices() {
	// Set the vertices for random triangles
	for(int i=0; i < 18; i++){
		mVertexJoints[i] = Vec3f(
			-1.0f + (float)rand()/((float)RAND_MAX/(1.0f+1.0f)),
			-1.0f + (float)rand()/((float)RAND_MAX/(1.0f+1.0f)),
			-0.25f + (float)rand()/((float)RAND_MAX/(0.25f+0.25f))
		);
		console() << "Vertices: " << mVertexJoints[i] << endl;
	}
}

// Run application
CINDER_APP_BASIC( KinectApp, RendererGl )
